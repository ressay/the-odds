# What are the odds

This project consists in a web application that computes and display the odds that the Millennium Falcon reaches Endor in time and saves the galaxy.

## Quick start and run the web app server

The web app runs using **Flask** server.

Install python requirements in `requirements.txt`. With `pip`:

``` python -m pip install -r requirements.txt ```

To run the app under development server:

```python -m flask --app . run```

The server normally runs on http://127.0.0.1:5000.

It is possible to change the port with the -p option:

```python -m flask --app . run -p 3000```


## Test the web app
It is possible to test the webapp by visiting the url where the flask server is running with your browser, e.g. http://127.0.0.1:5000.

This gives you access to the basic app which only displays the probability of reaching the goal and the steps required to do it given the uploaded `empire.json` file.

![Nograph](resources/nograph.png)

It is possible to add /graph to visualise the planet routes, e.g. http://127.0.0.1:5000/graph:

![Graph](resources/graph.png)


## Test the CLI

It is possible to test the program that computes the probability of success directly from the command line by running:

``` python3 give_me_the_odds.py path/to/millennium-falcon.json path/to/empire1.json  ```

For example:

```python3 give_me_the_odds.py data/millennium-falcon.json tests/empire1.json ```


