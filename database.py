# coding: utf-8
import sqlite3


def get_db(path: str) -> sqlite3.Connection:
    db = sqlite3.connect(
        "file:"+path+"?mode=ro",
        detect_types=sqlite3.PARSE_DECLTYPES,
        uri=True
    )
    db.row_factory = sqlite3.Row

    return db


def close_db(db: sqlite3.Connection):
    if db is not None:
        db.close()


def get_all_routes(db: sqlite3.Connection):
    routes = db.execute("SELECT * FROM routes").fetchall()
    return routes