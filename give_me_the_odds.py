import database
from graph import build_graph_from_rows
import heapq
from math import pow
import json
import argparse
import os

"""
searches for best path to reach goal from start
returns path from start to goal (list), days to reach it (int), and probability to reach it (float)
"""


def graph_search(start: str, goal: str, graph: dict, hunted_planets: dict, autonomy: int, countdown: int) -> \
        (list, int, float):
    n_path = []

    class Node(object):
        def __init__(self, name: str, days: int, captures: int, auto: int, parent=None):
            self.name = name
            self.days = days
            self.captures = captures
            self.auto = auto
            self.parent = parent

        def __lt__(self, other):
            # compare probabilities first
            if self.captures < other.captures:
                return True
            if other.captures < self.captures:
                return False
            # equal probas, compare days
            if self.days < other.days:
                return True
            return False

        def actions(self):
            return graph[self.name]

        def __str__(self):
            return self.name + " d: " + str(self.days) + " c: " + str(self.captures) + " a: " + str(self.auto)

    def end(h: list) -> bool:
        return len(h) == 0 or h[0].name == goal

    s = Node(start, 0, 0, autonomy)
    heap = [s]
    seen = set()
    while not end(heap):
        # get node with minimum days that has the best success probability
        n = heapq.heappop(heap)

        if n.days > countdown:
            # TODO manage error
            print("ERROR, there should not be n.days > countdown in heap")

        acts = n.actions()  # get possible move actions (wait action will be handled later)
        for a in acts:
            if acts[a] > n.auto:  # do not have autonomy to reach destination of action a
                continue
            d = n.days + acts[a]  # total days to reach
            c = n.captures  # number of times passed through bounty hunters
            if a in hunted_planets and d in hunted_planets[a]:
                c += 1

            n2 = Node(a, d, c, n.auto - acts[a], n)
            if (n2.name, n2.captures, n2.days, n2.auto) in seen or n2.days > countdown:
                continue

            seen.add((n2.name, n2.captures, n2.days, n2.auto))
            heapq.heappush(heap, n2)

        # add wait action
        c = n.captures
        if n.name in hunted_planets and (n.days + 1) in hunted_planets[n.name]:
            c += 1
        if (n.name, c, n.days + 1, autonomy) not in seen and n.days + 1 <= countdown:
            heapq.heappush(heap, Node(n.name, n.days + 1, c, autonomy, n))

    if len(heap) == 0:  # impossible to reach goal in time
        return [], -1, 0
    n = heap[0]  # goal reached at this node.
    proba = 1 - sum([pow(9, i - 1) / pow(10, i) for i in range(1, n.captures + 1)])  # compute its probability
    days = n.days
    while n is not None:  # get path from start to goal
        n_path = [(n.name, n.days, n.captures, n.auto)] + n_path
        n = n.parent
    return n_path, days, proba


def get_graph(configs: dict) -> dict:
    return build_graph_from_rows(database.get_all_routes(database.get_db(configs['routes_db'])))


"""
reformat hunters 
from [{planet: p1, day: y1}, {planet: p2, day: y2}, ...] 
to {p1: {y1, y4, ..}, p2: {y2, ...}, ...}
"""


def reformat_hunters(empire: dict) -> dict:
    new_hunters = {}
    hunters = empire["bounty_hunters"]
    for hunter in hunters:
        p, d = hunter["planet"], hunter["day"]

        if p not in new_hunters:
            new_hunters[p] = set()

        new_hunters[p].add(d)

    return new_hunters


def give_me_the_odds(configs: dict, empire: dict) -> (list, int, float):
    graph = get_graph(configs)
    hunters = reformat_hunters(empire)
    countdown = empire["countdown"]
    return graph_search(configs['departure'], configs['arrival'], graph,
                        hunters, configs['autonomy'], countdown)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='''Finds the probability that Millennium Falcon reaches its goal in time and saves the galaxy!''')
    parser.add_argument('millennium_falcon', help='path to .json file containing values for: autonomy, '
                                                  'departure, arrival and routes_db')
    parser.add_argument('empire', help='path to .json file containing values for: countdown and bounty_hunters ')
    args = parser.parse_args()

    try:
        configs = json.load(open(args.millennium_falcon))
    except Exception as e:
        print("ERROR: could not parse " + args.millennium_falcon)
        parser.print_help()
        exit(1)

    try:
        empire = json.load(open(args.empire))
    except Exception as e:
        print("ERROR: could not parse " + args.empire)
        parser.print_help()
        exit(1)

    path = os.path.dirname(args.millennium_falcon)
    if not os.path.isabs(configs["routes_db"]):
        configs["routes_db"] = os.path.realpath(os.path.join(path, configs["routes_db"]))

    try:
        path, d, p = give_me_the_odds(configs, empire)
    except Exception as e:
        print("ERROR: the following error occured ")
        print(e)
        parser.print_help()
        exit(1)

    print(int(p * 100))
