

"""
checks if edge between o and d exist in graph (enough to check one direction)
"""
def in_graph(o: str, d: str, g: dict) -> bool:
    return o in g and d in g[o]


"""
returns cost of travel if couple (o,d) exists in graph and -1 otherwise (constraint in db that cost>0)
"""
def get_from_graph(o: str, d: str, g: dict) -> int:
    if in_graph(o, d, g):
        return g[o][d]
    return -1


"""
adds edge to graph from o to d only (updates it with lower cost value if it already exist)
"""
def add_to_graph_unique(o: str, d: str, c: int, g: dict):
    if o not in g:
        g[o] = {}
    if d not in g[o] or c < g[o][d]:
        g[o][d] = c


"""
adds edge to graph in both directions
"""
def add_to_graph(o: str, d: str, c: int, g: dict):
    add_to_graph_unique(o, d, c, g)
    add_to_graph_unique(d, o, c, g)


def build_graph_from_rows(rows: list) -> dict:
    graph = {}
    for row in rows:
        o, d, c = row['origin'], row['destination'], row['travel_time']
        add_to_graph(o, d, c, graph)

    return graph
