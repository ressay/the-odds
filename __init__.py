import os

from flask import Flask, render_template, request, abort
import json
from give_me_the_odds import give_me_the_odds, get_graph

CONFIGS_PATH = "data/millennium-falcon.json"


def create_app():

    app = Flask(__name__)

    configs_loaded = True
    configs_well_defined = True
    try:
        file = open(CONFIGS_PATH)
        configs = json.load(file)
        path = os.path.dirname(CONFIGS_PATH)
        if not os.path.isabs(configs["routes_db"]):
            configs["routes_db"] = os.path.realpath(os.path.join(path, configs["routes_db"]))
    except Exception as e:
        app.logger.error("Could not load " + CONFIGS_PATH)
        app.logger.error(e)
        configs_loaded = False

    try:
        graph = get_graph(configs)
        if not isinstance(configs["autonomy"], int):
            configs_well_defined = False
            app.logger.error("countdown must be an integer value")

        if configs["departure"] not in graph:
            configs_well_defined = False
            app.logger.error("departure " + configs["departure"] + " does not exist in database")

        if configs["arrival"] not in graph:
            configs_well_defined = False
            app.logger.error("arrival " + configs["arrival"] + " does not exist in database")

    except Exception as e:
        app.logger.error(e)
        configs_well_defined = False




    """
        VIEWS
    """

    @app.route('/')
    def home():
        return app.redirect(app.url_for("nogr"))

    @app.route('/nograph')
    def nogr():
        if not configs_loaded or not configs_well_defined:
            abort(500)
        return render_template("index.html", graph=0)

    @app.route('/graph')
    def gr():
        if not configs_loaded or not configs_well_defined:
            abort(500)
        return render_template("index.html", graph=1)

    @app.route('/error')
    def error():
        return render_template("error.html")

    @app.errorhandler(404)
    def handle404(e):
        app.logger.error(e)
        return render_template("error.html", err=404, message="Not Found"), 404

    @app.errorhandler(403)
    def handle403(e):
        app.logger.error(e)
        return render_template("error.html", err=403, message="Forbidden"), 403

    @app.errorhandler(400)
    def handle410(e):
        app.logger.error(e)
        return render_template("error.html", err=400, message="Bad request"), 400

    @app.errorhandler(500)
    def handle500(e):
        app.logger.error(e)
        return render_template("error.html", err=500, message="Internal Server Error"), 500

    @app.errorhandler(500)
    def handle500(e):
        app.logger.error(e)
        return render_template("error.html", err=500), 500

    """
        REQUESTS
    """
    @app.route('/search', methods=['POST'])
    def get_post_javascript_data():
        if not configs_loaded or not configs_well_defined:
            abort(500)
        try:
            empire = request.get_json()
            path, d, p = give_me_the_odds(configs, empire)
        except Exception as e:
            app.logger.error(e)
            abort(400)
        return {
            "path": path,
            "days_to_reach": d,
            "probability": p
        }

    @app.route('/getg', methods=['POST'])
    def get_g():
        if not configs_loaded or not configs_well_defined:
            abort(500)

        new_graph = {
            "nodes": [{"id": n} for n in graph],
            "edges": []
        }
        exist = set()
        for n in graph:
            for n2 in graph[n]:
                if (n2, n) not in exist:
                    exist.add((n, n2))
                    new_graph["edges"].append({"from": n, "to": n2, "info": graph[n][n2]})
        return new_graph

    return app
