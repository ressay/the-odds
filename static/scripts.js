

        function readSingleFile(e) {
          var file = e.target.files[0];
          if (!file) {
            return;
          }
          var reader = new FileReader();
          reader.onload = function(e) {
            try {
            var empire = JSON.parse(e.target.result);
            } catch (error) {
                alert(error);
                exit()
            }
            $.ajax({
            type: 'POST',
            url: '/search',
            data: JSON.stringify(empire),
            contentType: 'application/json',
            dataType : 'json',
            success: function(contents){ displayContents(contents); },
            error: function(contents){ alert("An error happened: bad request");}
            });

          };
          reader.readAsText(file);
        }

        function nodesToText(n1, n2) {
            var str = "";
            if(n1[0] === n2[0])
                if(n1[3] == n2[3])
                    str += "Wait in " + n1[0] + " for one day";
                else
                    str += "Refuel on " + n1[0];
            else
                str += "Travel from " + n1[0] + " to " + n2[0];

            if(n1[2] != n2[2])
                str += ", with 10% chance of being captured on day " + n2[1] + " on " + n2[0] + ".";
            return str;
        }



        function displayContents(contents) {
          path = contents.path
          var element = document.getElementById('file-content');
          element.innerHTML="";
          var captured = document.getElementById('captured');

          if(contents["probability"] > 0) {
              captured.innerHTML="The probability of reaching " + path.slice(-1)[0][0] + " is " + (contents["probability"]*100) + "%";
              var tbl = element;


              for (let i = 0; i < path.length-1; i++) {
                const tr = tbl.insertRow();
                const td = tr.insertCell();
                td.appendChild(document.createTextNode(nodesToText(path[i], path[i+1])));
              }
              // update graph if it exists
                var event =  new CustomEvent("updateGraph", {"detail": path});
                document.dispatchEvent(event);


          }
          else {
              captured.innerHTML="The probability of reaching goal is 0%";
              var event =  new CustomEvent("updateGraphFail");
                document.dispatchEvent(event);
          }
        }

window.addEventListener('DOMContentLoaded', function() {
    document.getElementById('file-input').addEventListener('change', readSingleFile, false);
});


