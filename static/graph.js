anychart.onDocumentReady(function () {
       var chart;
       var g_data;

       function create_graph(data) {
            // create a chart and set the data
            chart = anychart.graph(data);

            // prevent zooming the chart with the mouse wheel
            chart.interactivity().zoomOnMouseWheel(true);

            // enable labels of nodes
            chart.nodes().labels().enabled(true);
            chart.nodes().labels().format(function() {
                if(this.getData("wait")) return this.id + "\nwait(" + this.getData("wait") + ")";
                return this.id;
            });

            // configure labels of nodes
            chart.nodes().labels().fontSize(20);
            chart.nodes().labels().fontWeight(600);

            chart.edges().tooltip().format(function() {
            return this.getData('info');
            });

            chart.edges().labels().enabled(true);
            chart.edges().labels().format(function() {
            return this.getData('info');
            });
            // configure labels of nodes
            chart.edges().labels().fontSize(20);
            chart.edges().labels().fontWeight(600);

            // configure tooltips of nodes
            chart.nodes().tooltip().useHtml(true);
            chart.nodes().height(30);

            // set the chart title
            chart.title("Routes base");

            document.getElementById('container').innerHTML = "";

            // set the container id
            chart.container("container");

            // initiate drawing the chart
            chart.draw();

            chart.zoom(0.8);
       }


        function updateGraph(path) {
            var p_nodes = [];
            for(var n in path) {
                p_nodes.push(path[n][0]);
            }
            var new_data = structuredClone(g_data);
            for(let i=0; i<new_data["nodes"].length;i++) {
                var n = new_data["nodes"][i];
                if(p_nodes.includes(n["id"]))
                    n["group"] = "passed";
            }

            var style = {
                normal: {stroke:  {
                         color: "#33ee33",
                         thickness: "3",
                         dash: "10 5",
                         lineJoin: "round"
                       }
                     },
                 hovered: {stroke: {
                     color: "#33ee33",
                     thickness: "5",
                     dash: "10 5",
                     lineJoin: "round"
                   }
                 },
                 selected: {stroke: "4 #ffa000"}
            };
            for(let i=0; i<p_nodes.length-1;i++) {
                var n = p_nodes[i], n2 = p_nodes[i+1];
                if(n === n2) {
                    nodes = new_data["nodes"];
                    for(let i in nodes)
                        if(nodes[i]["id"] === n) {
                            if(!("wait" in nodes[i]))
                                nodes[i]["wait"] = 0;
                            nodes[i]["wait"] += 1;
                        }
                    continue;
                }
                edges = new_data["edges"]
                for(var e in edges) {
                    if((edges[e]["from"] === n && edges[e]["to"] === n2) || (edges[e]["from"] === n2 && edges[e]["to"] === n)) {
                        edges[e]["normal"] = style.normal;
                        edges[e]["hovered"] = style.hovered;
                        edges[e]["selected"] = style.selected;
                    }
                }
            }

            create_graph(new_data);

        }

        function updateGraphListener(event) {
            updateGraph(event.detail);
        }

        function updateGraphFailListener(event) {
            var new_data = structuredClone(g_data);
            create_graph(new_data)
            chart.nodes().fill("#ee3333");
            chart.edges().stroke({
                color: '#ee5555',
                thickness: '2',
                dash: '6 3'
              });
        }


        $.ajax({
            type: 'POST',
            url: '/getg',
            success: function(content){
            g_data = content
            create_graph(g_data)
            },
            error: function(content) { alert("An error happened: bad request");}
        });

        document.addEventListener('updateGraph', updateGraphListener, false);
        document.addEventListener('updateGraphFail', updateGraphFailListener, false);

       });